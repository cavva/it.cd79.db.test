package it.cd79.db.test.measure;

import java.sql.ResultSet;

import it.cd79.db.test.exceptions.ExecutionCalcException;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class MeasureTime {
	private long start;
	private long end;
	private ResultSet result;

	/**
	 * Execution time
	 *
	 * @return end - start
	 * @throws ExecutionCalcException
	 *             if start or end is not declared
	 */
	public long executionTime() throws ExecutionCalcException {
		if (start < 1)
			throw new ExecutionCalcException("Start MUST be set");
		if (end < 1)
			throw new ExecutionCalcException("End MUST be set");
		if (start < end)
			return end - start;
		return start - end;
	}
}
