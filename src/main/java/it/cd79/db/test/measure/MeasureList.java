package it.cd79.db.test.measure;

import java.util.List;

import com.google.common.collect.Lists;

import it.cd79.db.test.exceptions.ExecutionCalcException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@Getter
@ToString(exclude="measures")
@EqualsAndHashCode
public class MeasureList {
	private List<MeasureTime> measures;

	private long min;
	private long max;
	private long total;


	public long getAvg() {

		return total / measures.size();

	}

	public void addMeasure(MeasureTime measure) throws ExecutionCalcException {
		if (measure == null)
			return;
		if (measures == null) {
			measures = Lists.newArrayList();
			min = Long.MAX_VALUE;
			max = 0;
			total = 0;
		}
		long executionTime = measure.executionTime();
		total += executionTime;
		if (min > executionTime)
			min = executionTime;
		if (max < executionTime)
			max = executionTime;
		measures.add(measure);
	}

}
