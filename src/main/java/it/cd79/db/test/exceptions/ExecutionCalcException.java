package it.cd79.db.test.exceptions;

public class ExecutionCalcException extends Exception {

	private static final long serialVersionUID = -4067631115716053762L;

	public ExecutionCalcException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExecutionCalcException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ExecutionCalcException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ExecutionCalcException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExecutionCalcException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
