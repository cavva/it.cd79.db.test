package it.cd79.db.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.api.callback.Callback;
import org.flywaydb.core.api.callback.Context;
import org.flywaydb.core.api.callback.Event;

public class InitSql implements Callback {

	private Logger logger;

	public InitSql() {
		logger = LogManager.getLogger(getClass());
	}

	@Override
	public boolean supports(Event event, Context context) {
		logger.info("Supports events : {}\n{}", event, context);
		return false;
	}

	@Override
	public boolean canHandleInTransaction(Event event, Context context) {
		logger.info("Can Handle In Transaction events : {}\n{}", event, context);
		return false;
	}

	@Override
	public void handle(Event event, Context context) {
		logger.info("Handle events : {}\n{}", event, context);
	}

}
