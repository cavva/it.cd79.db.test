package it.cd79.db.test.flyway;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class FlywayConfig {
	private String url;
	private String user;
	private String pwd;
	private String locations;
	private Boolean outOfOrder;
	private boolean active;

}
