package it.cd79.db.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.flywaydb.core.Flyway;

import it.cd79.db.test.exceptions.ExecutionCalcException;
import it.cd79.db.test.flyway.FlywayConfig;
import it.cd79.db.test.measure.MeasureList;
import it.cd79.db.test.measure.MeasureTime;
import it.cd79.db.test.measure.MeasureTime.MeasureTimeBuilder;

public class Main {

	private static Main instance;
	private Logger logger;

	private Properties configs;

	private Flyway flyway;
	private Connection connection;

	public static void main(String[] args) throws IOException, URISyntaxException {
		instance = new Main();
		instance.initConfiguration();
		instance.initDb();
		instance.initTables();
		MeasureList inserts = instance.insertStatements();
		MeasureList selects = instance.selectStatements();
		instance.printResults(inserts, selects);

	}

	public Main() {
		logger = LogManager.getLogger(getClass());

		flyway = new Flyway();
		configs = new Properties();
	}

	private void initConfiguration() throws IOException {
		configs.load(this.getClass().getResourceAsStream("/configuration.properties"));
	}

	private void initDb() throws URISyntaxException {

		FlywayConfig config = FlywayConfig.builder().url(configs.getProperty("jdbc.init.url"))
				.user(configs.getProperty("jdbc.user")).pwd(configs.getProperty("jdbc.password"))
				.locations(configs.getProperty("flyway.init.location"))
				.outOfOrder(Boolean.valueOf(configs.getProperty("flyway.migrations.outOfOrder")))
				.active(Boolean.valueOf(configs.getProperty("flyway.active")))
				.build();
		migrate(config);
	}

	private void initTables() throws URISyntaxException {

		FlywayConfig config = FlywayConfig.builder().url(configs.getProperty("jdbc.url"))
				.user(configs.getProperty("jdbc.user")).pwd(configs.getProperty("jdbc.password"))
				.locations(configs.getProperty("flyway.location"))
				.outOfOrder(Boolean.valueOf(configs.getProperty("flyway.migrations.outOfOrder")))
				.active(Boolean.valueOf(configs.getProperty("flyway.active")))
				.build();
		migrate(config);
	}

	private void migrate(FlywayConfig config) {
		if (!config.isActive())
			return;

		flyway.setDataSource(config.getUrl(), config.getUser(), config.getPwd());
		flyway.setOutOfOrder(config.getOutOfOrder());
		if (StringUtils.isNotBlank(config.getLocations())) {
			flyway.setLocations(config.getLocations().split(","));
		}
		flyway.migrate();
	}

	private Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(configs.getProperty("jdbc.url"), configs.getProperty("jdbc.user"),
					configs.getProperty("jdbc.password"));
			connection.setAutoCommit(false);
		}
		return connection;
	}

	private MeasureTime query(String statement, Object var) {
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = getConnection();
			ps = con.prepareStatement(statement);

			if (var instanceof String)
				ps.setString(1, (String) var);
			else if (var instanceof Long)
				ps.setLong(1, (Long) var);

			long start = System.nanoTime();
			boolean hasExecuted = ps.execute();
			long end = System.nanoTime();
			MeasureTimeBuilder measure = MeasureTime.builder().start(start).end(end);
			if (hasExecuted) {
				ResultSet result = ps.getResultSet();
				measure.result(result);
			}
			return measure.build();

		} catch (SQLException e) {
			logger.info("Transaction is being rolled back", e);
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					logger.error("", excep);
				}
			}
			return null;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					logger.error("Error closing preparedStatement", e);
				}
			}
		}

	}

	private MeasureList insertStatements() {
		String insertQuery = "insert into test_table (\"text\")\n" + "values (?)";
		MeasureList measures = new MeasureList();

		int tuplesToInsert = Integer.parseInt(configs.getProperty("jdbc.inserts.random.tuples"));
		int commitAfter = Integer.parseInt(configs.getProperty("jdbc.queries.before.commit"));

		for (int i = 0; i < tuplesToInsert; i++) {
			String randomText = null;
			try {
				randomText = RandomStringUtils.random(40, true, true);
				MeasureTime measure = query(insertQuery, randomText);
				measures.addMeasure(measure);
				if (i % commitAfter == 0)
					getConnection().commit();
			} catch (ExecutionCalcException e) {
				logger.error(new FormattedMessage("Error inserting the tuple '{}'", randomText), e);
			} catch (SQLException e) {
				logger.error("Commit error on insert", e);
			}
		}
		logger.info("Insert statement\n\tmin: {}ns\n\tmax: {}ns\n\tavg: {}ns", measures.getMin(), measures.getMax(),
				measures.getAvg());
		return measures;

	}

	private MeasureList selectStatements() {
		String selectQuery = "select * from test_table where id = ?";
		MeasureList measures = new MeasureList();

		int tuplesToInsert = Integer.parseInt(configs.getProperty("jdbc.selects.random.tuples"));
		int commitAfter = Integer.parseInt(configs.getProperty("jdbc.queries.before.commit"));

		for (int i = 0; i < tuplesToInsert; i++) {
			Long randomLong = null;
			try {
				randomLong = RandomUtils.nextLong(0,
						Long.parseLong(configs.getProperty("jdbc.inserts.random.tuples")) + 1l);
				measures.addMeasure(query(selectQuery, randomLong));
				if (i % commitAfter == 0)
					getConnection().commit();
			} catch (ExecutionCalcException e) {
				logger.error(new FormattedMessage("Error inserting the tuple '{}'", randomLong), e);
			} catch (SQLException e) {
				logger.error("Commit error on insert", e);
			}
		}
		logger.info("Select statement\n\tmin: {}ns\n\tmax: {}ns\n\tavg: {}ns", measures.getMin(), measures.getMax(),
				measures.getAvg());
		return measures;

	}

	private void printResults(MeasureList inserts, MeasureList selects) {
		System.out.println("---------- Inserts Execution time ----------");
		inserts.getMeasures().forEach(item -> {
			try {
				System.out.println(String.format("Insert Execution Time %d ns", item.executionTime()));
			} catch (Exception e) {
				logger.error("Error calculating execution time", e);
			}
		});
		System.out.println("---------- Selects Execution time ----------");
		selects.getMeasures().forEach(item -> {
			try {
				System.out.println(String.format("Select Execution Time %d ns", item.executionTime()));
			} catch (Exception e) {
				logger.error("Error calculating execution time", e);
			}
		});

		System.out.println(String.format("Insert\n\tmin: %d ns\n\tmax: %d ns\n\tavg: %d ns", inserts.getMin(),
				inserts.getMax(), inserts.getAvg()));
		System.out.println(String.format("Select\n\tmin: %d ns\n\tmax: %d ns\n\tavg: %d ns", selects.getMin(),
				selects.getMax(), selects.getAvg()));
	}
}
