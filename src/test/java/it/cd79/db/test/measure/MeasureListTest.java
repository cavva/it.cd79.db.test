package it.cd79.db.test.measure;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.cd79.db.test.exceptions.ExecutionCalcException;

public class MeasureListTest {

	private MeasureList measures;

	@BeforeEach
	public void initTests() throws ExecutionCalcException {
		measures = new MeasureList();
		measures.addMeasure(MeasureTime.builder().start(10000l).end(20000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(30000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(40000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(75000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(70000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(15000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(12000l).build());
		measures.addMeasure(MeasureTime.builder().start(10000l).end(80000l).build());
		// 262.000 ms
		// 8 elements
	}

	@Test
	public void testGetAvg() throws Exception {
		assertEquals(32750l, measures.getAvg());
	}

	@Test
	public void testGetMin() throws Exception {
		assertEquals(70000l, measures.getMax());
	}

	@Test
	public void testGetMax() throws Exception {
		assertEquals(2000l, measures.getMin());
	}

	@Test
	public void testAddMeasureStartAndEndNotDeclared() throws Exception {
		assertThrows(ExecutionCalcException.class, () -> {
			measures.addMeasure(MeasureTime.builder().build());
		});
	}

	@Test
	public void testAddMeasureStartNotDeclared() throws Exception {
		assertThrows(ExecutionCalcException.class, () -> {
			measures.addMeasure(MeasureTime.builder().end(2000l).build());
		});
	}

	@Test
	public void testAddMeasureEndNotDeclared() throws Exception {
		assertThrows(ExecutionCalcException.class, () -> {
			measures.addMeasure(MeasureTime.builder().start(20000l).build());
		});
	}

	@Test
	public void testAddMeasureStartAndEndDeclared() throws Exception {
		measures.addMeasure(MeasureTime.builder().start(20000l).end(30000l).build());
	}

}
