package it.cd79.db.test.measure;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import it.cd79.db.test.exceptions.ExecutionCalcException;

public class MeasureTimeTest {

	@Test
	public void testExecutionTimeAllDecalred() throws Exception {
		assertEquals(3l, MeasureTime.builder().start(12l).end(15l).build().executionTime());
	}

	@Test
	public void testExecutionTimeAllDecalredInverted() throws Exception {
		assertEquals(3l, MeasureTime.builder().start(15l).end(12l).build().executionTime());
	}

	@Test
	public void testExecutionTimeStartDecalred() throws Exception {
		assertThrows(ExecutionCalcException.class, ()->{
			MeasureTime.builder().start(12l).build().executionTime();
		});
	}

	@Test
	public void testExecutionTimeEndDecalred() throws Exception {
		assertThrows(ExecutionCalcException.class, ()->{
			MeasureTime.builder().end(15l).build().executionTime();
		});
	}


}
