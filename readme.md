# dB performance tests

This project aims to test min/max/avg time to make insert statements and select over pk.

## Prepare working environment

I'm using lombok project to remove boilerplate code from classes.
If run in eclipse, it requires lombok to be installed.
Second step is to initialize docker image for postgreSql if you don't
want to install the dB on your machine, and start it (docker-composer up also start the dB container).

### Installing lombok in eclipse

From project folder run:

```shell
mvn dependency:copy-dependencies
```

It will copy also lombok.jar on target/dependency folder.
To install lombok in ecplise run:
```shell
java -jar target/dependency/lombok-1.16.22.jar
```

Select "specify location..." and select eclipse installation directory, then "install/update".

### Inizializing dockerised postgreSql 10.4

From project folder run:

```shell
docker-composer up
```

### Using dockerised postgreSql 10.4

From project folder run:

```shell
docker-composer start
```

#### Stop dockerized postgreSql 10.4

From project folder run:

```shell
docker-composer stop
```

#### Removing dockerized postgreSql 10.4

From project folder run:

```shell
docker-composer down
```

## Running the project

This proof of concept uses "Flyway" to initialize the dB.
The only things it needs it is configuration.properties well written.
Flyway itself take care of create db and tables to test.

### Configuration

| Property                     | Description                                        |
| ---------------------------- | -------------------------------------------------- |
| jdbc.init.url                | Url Jdbc for CREATE DATABASE                       |
| jdbc.url                     | Url Jdbc for normal usage of the created database  |
| jdbc.user                    | Username for db connection with create db role too |
| jdbc.password                | dB User password                                   |
| flyway.active                | Ativate Flyway for automatic dB and table creation |
| flyway.init.location         | Sql files location for dB initialization           |
| flyway.location              | Sql files location for DDLs                        |
| flyway.migrations.outOfOrder | Migration out of order                             |
| jdbc.queries.before.commit   | Queries before commit                              |
| jdbc.inserts.random.tuples   | Number of random data to insert                    |
| jdbc.selects.random.tuples   | Number of random select to perfom                  |